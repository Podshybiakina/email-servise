public with sharing class EmailTemplatesDescriber {

  @AuraEnabled//(cacheable = true)
  public static Map<String, String> getTemplateFolders() {
    List<EmailTemplate> folders = [
      SELECT FolderId, Folder.Name
      FROM EmailTemplate
      ORDER BY Folder.Name ASC
      LIMIT 50000
    ];
    Map<String, String> folderNameToId = new Map<String, String>();

    if (!folders.isEmpty()) {
      for (EmailTemplate template : folders) {
        folderNameToId.put(template.FolderId, template.Folder.Name);
      }
    }

    return folderNameToId;
  }

  @AuraEnabled//(cacheable = true)
  public static Map<String, String> getTemplates(String folderId) {
    Map<String, String> templatesNameToId = new Map<String, String>();

    if (String.isNotEmpty(folderId)) {
      List<EmailTemplate> templates = Database.query(
        'SELECT Id, Name, FolderId ' +
        'FROM EmailTemplate ' +
        'WHERE FolderId = :folderId ' +
        'LIMIT 10000'
      );

      for (EmailTemplate template : templates) {
        templatesNameToId.put(template.Id, template.Name);
      }
    }
    return templatesNameToId;
  }

  @AuraEnabled//(cacheable = true)
  public static Map<String, String> getTemplateBody(String templateId) {
    if (String.isEmpty(templateId)) {
      return null;
    }

    Map<String, String> templateData = new Map<String, String>();
    List<EmailTemplate> template = Database.query(
      'SELECT Id, Name, HtmlValue, Body, Markup, IsActive, TemplateType, Subject ' +
      'FROM EmailTemplate ' +
      'WHERE Id = :templateId ' +
      'LIMIT 10000'
    );

    templateData.put('isactive', String.valueOf(template[0].IsActive));
    templateData.put('name', template[0].Name);
    templateData.put('type', template[0].TemplateType);

    if (template[0].TemplateType == 'visualforce') {
      templateData.put('subject', template[0].Markup.substringBetween('subject="', '"'));
    } else {
      templateData.put('subject', template[0].Subject);
    }

    if (template.isEmpty()) {
      String listIsEmpty = 'Template not found';
      templateData.put('Error',listIsEmpty);
    } else if (String.isEmpty(template[0].HtmlValue) && String.isNotEmpty(template[0].Body)) {
      templateData.put('body', template[0].Body);
    } else if (String.isNotEmpty(template[0].Markup)) {
      templateData.put('body',template[0].Markup);
      templateData.put('recipient', template[0].Markup.substringBetween('recipientType="', '"'));
    } else if (String.isEmpty(template[0].Body) && String.isEmpty(template[0].HtmlValue)) {
      templateData.put('Error','Template don\'t have a body');
    } else {
      templateData.put('body', template[0].HtmlValue);
    }

    System.debug('templateData: '+templateData);
    return templateData;
  }

  @AuraEnabled
  public static String updateTemplate(Map<String, String> updatedTemplate) {
    EmailTemplate template = [
      SELECT Id, Name, HtmlValue, Body, Markup, TemplateType, IsActive, Subject
      FROM EmailTemplate
      WHERE Id = :updatedTemplate.get('id')
    ];

    System.debug('updatedTemplate: '+updatedTemplate);

    Boolean isTextType = template.TemplateType == 'text';
    Boolean isCustomType = template.TemplateType == 'custom';
    Boolean isHtmlType = template.TemplateType == 'html';
    Boolean isVisualforceType = template.TemplateType == 'visualforce';
    String templateMarkup =
      '<messaging:emailTemplate subject="#SUBJECT" recipientType="#RECIPIENTTYPE" >' +
        '<messaging:plainTextEmailBody >'+
        '#BODY'+
        '</messaging:plainTextEmailBody>'+
        '</messaging:emailTemplate>';

    template.Name = updatedTemplate.get('name');

    if (updatedTemplate.get('isactive').equalsIgnoreCase('true')) {
      template.IsActive = true;
    } else {
      template.IsActive = false;
    }

    if ((isCustomType || isHtmlType) && updatedTemplate.get('body').trim().startsWith('<')) {
      template.HtmlValue = updatedTemplate.get('body');
      template.Body = updatedTemplate.get('body').replaceAll('</p>', '\n').replaceAll('<[^>]*>', '');
      template.Subject = updatedTemplate.get('subject');
    } else if (isVisualforceType) {
      templateMarkup = templateMarkup.replace('#BODY', updatedTemplate.get('body').replaceAll('</p>', '\n').replaceAll('<[^>]*>', ''));
      templateMarkup = templateMarkup.replace('#RECIPIENTTYPE', updatedTemplate.get('recipient'));
      template.Markup = templateMarkup.replace('#SUBJECT', updatedTemplate.get('subject'));
    } else {
      template.Body = updatedTemplate.get('body').replaceAll('</p>', '\n').replaceAll('<br>', '\n').replaceAll('<[^>]*>', '');
      template.Subject = updatedTemplate.get('subject');
    }

    System.debug('template.Body: '+template.Body);

    try {
      update template;
      return 'Template updated.';
    } catch (Exception e) {
      return e.getMessage();
    }
  }

  @AuraEnabled
  public static String createTemplate(Map<String, String> newTemplate) {
    System.debug('newTemplate: '+newTemplate);
    System.debug('newTemplateBody: '+newTemplate.get('body'));
    Boolean isActive = false;
    String templateBody = '';
    String templateHtml = '';
    String templateType = 'custom';
    String uniqueName = newTemplate.get('name').trim().replaceAll('\\s', '_');
    String templateMarkup =
      '<messaging:emailTemplate subject="#SUBJECT" recipientType="#RECIPIENTTYPE" >' +
      '<messaging:plainTextEmailBody > #BODY </messaging:plainTextEmailBody>'+
      '</messaging:emailTemplate>';

    if (newTemplate.get('type').containsIgnoreCase('custom')) {
      templateHtml = newTemplate.get('body');
      templateBody = newTemplate.get('body').replaceAll('</p>', '\n').replaceAll('<[^>]*>', '');
    } else if (newTemplate.get('type').equalsIgnoreCase('text')) {
      templateType = 'text';
      templateBody = newTemplate.get('body').replaceAll('</p>', '\n').replaceAll('<[^>]*>', '');
    } else if (newTemplate.get('type').equalsIgnoreCase('visualforce')) {
      templateType = 'visualforce';
      templateMarkup = templateMarkup.replace('#SUBJECT', newTemplate.get('subject'));
      templateMarkup = templateMarkup.replace('#BODY', newTemplate.get('body'));
      templateMarkup = templateMarkup.replace('#RECIPIENTTYPE', newTemplate.get('recipient'));
    } else {
      templateHtml = newTemplate.get('body');
    }

    if ((newTemplate.get('isactive')).equalsIgnoreCase('true')) {
      isActive = true;
    }

    EmailTemplate template = new EmailTemplate(
      FolderId = newTemplate.get('folderid'),
      TemplateType = templateType,
      Name = newTemplate.get('name'),
      DeveloperName = uniqueName,
      Subject = newTemplate.get('subject'),
      Body = templateBody,
      HtmlValue = templateHtml,
      IsActive = isActive
    );

    if (newTemplate.get('type').equalsIgnoreCase('visualforce')) {
      template.Markup = templateMarkup;
    } else if (newTemplate.get('type').equalsIgnoreCase('html')) {
      template.BrandTemplateId = newTemplate.get('letterheadId');
    }

    System.debug('template: '+template); System.debug('templateBody: '+templateBody);
    try {
      insert template;
      return 'Template created.';
    } catch (Exception e) {
      return e.getMessage();
    }
  }

  @AuraEnabled
  public static String deleteTemplate(String templateId) {
    System.debug('templateId: '+templateId);
    List<EmailTemplate> templatesToDelete = [SELECT Id FROM EmailTemplate WHERE Id = :templateId];

    try {
      delete templatesToDelete;
      return 'Template deleted.';
    } catch (Exception e) {
      return e.getMessage();
    }
  }

  @AuraEnabled
  public static List<String> getObjectNames() {
    List<String> whatIdObjectNames = new List<String>{
      'Account', 'Contact', 'Asset', 'Campaign', 'Case', 'Contract', 'Opportunity', 'Order', 'Product', 'Solution'
    };
    for(Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
      Schema.DescribeSObjectResult customObjResult = o.getDescribe();
      if (customObjResult.isCustom()) {
        whatIdObjectNames.add(customObjResult.getName());
      }
    }
    return whatIdObjectNames;
  }

  @AuraEnabled
  public static Map<String, SObject> getMergeData(String selectedObject) {
    Map<String, SObject> outputData = new Map<String, SObject>();
    String queryString = 'SELECT Id, Name FROM ' + selectedObject;

    if (selectedObject.equalsIgnoreCase('Case')) {
      queryString = queryString.replace('Name', 'CaseNumber, Description, Status, Subject, SuppliedName ');
    } else if (selectedObject.equalsIgnoreCase('Solution')) {
      queryString = queryString.replace('Name', 'SolutionName ');
    }

    System.debug('queryString: '+queryString);
    List<SObject> objectList = Database.query(queryString);
    for (SObject obj : objectList) {
      outputData.put((String)obj.Id, obj);
    }
    return outputData;
  }

  @AuraEnabled
  public static String getTemplatePreview(Map<String, String> renderData) {
    String currentUserId = UserInfo.getUserId();
    Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(
      renderData.get('templateId'), renderData.get('recipientId'), renderData.get('relatedRecId')
    );

    if (String.isNotEmpty(renderData.get('relatedRecId'))) {
      email.setWhatId(renderData.get('relatedRecId'));
    }

    System.debug('renderData: '+renderData);
    List<String> toAddresses = new List<String>();
    if (String.isNotEmpty(renderData.get('testemail')) && (renderData.get('send') == 'true')) {
      toAddresses.add(renderData.get('testemail'));
      email.setToAddresses(toAddresses);
      email.setSaveAsActivity(true);
      Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }

    System.debug('email: '+email);

    if (String.isNotEmpty(email.getHtmlBody())) {
      return email.getHtmlBody();
    } else {
      return email.getPlainTextBody();
    }
  }

  @AuraEnabled
  public static List<String> sendEmail(Map<String, String> renderData) {
    String currentUserId = UserInfo.getUserId();
    Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(
      renderData.get('templateId'), currentUserId, renderData.get('recipientId')
    );
    email.setSaveAsActivity(true); 
    Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    List<String> successList = new List<String>();
    List<String> errorsList = new List<String>();
    List<Messaging.SendEmailError> errors = new List<Messaging.SendEmailError>();

    for (Messaging.SendEmailResult result : results) {
      if (result.isSuccess() == true) {
        successList.add('SUCCESS');
      } if (result.isSuccess() == false) {
        errors.addAll(result.getErrors());
      }
    }

    if (!errors.isEmpty()) {
      for (Messaging.SendEmailError error : errors) {
        errorsList.add(error.getMessage());
      }
      return errorsList;
    } else {
      return successList;
    }
  }

  @AuraEnabled
  public  static Map<String, String> getLetterheads() {
    Map<String, String> letterheadNameToId = new Map<String, String>();
    List<BrandTemplate> letterheads = [SELECT Id, Name FROM BrandTemplate];
    for (BrandTemplate letterhead : letterheads) {
      letterheadNameToId.put(letterhead.Id, letterhead.Name);
    }
    return letterheadNameToId;
  }

  public static String verifyMergeFields(String templateId, String recipientId, String relatedRecordId) {

    return null;
  }
}