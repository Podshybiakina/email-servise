({
	getTemplateFolders : function (component) {
		var action = component.get("c.getTemplateFolders");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                this.populateTemplateFolders(component, response.getReturnValue());
            } else if (state === 'ERROR') {
               var errors = response.getError();
               console.log(errors);
           }
        });
        $A.enqueueAction(action);
    },

    populateTemplateFolders : function (component, response) {
        var templateFolders = [];
        var availebleFolders = [{id : "", name : "Select a folder"}];
        Object.keys(response).forEach(function(folder) {
            templateFolders.push({
                "label" : response[folder],
                "name" : folder,
                "disabled" : false,
                "expanded" : true,
                "items": []
            });
            availebleFolders.push({
                'id' : folder,
                "name" : response[folder]
            });
        });
        component.set('v.items', templateFolders);
        component.set('v.availebleFolders', availebleFolders);
    },

    handleSelect : function (component, event) {
        component.set('v.isTemplateForEditShown', false);

        var existedTemplateFolders = this.parseIfProxy(component.get('v.items'));
        var selectedItemId = event.getParam('name');
        var isSubfolderNeeded = false;

        existedTemplateFolders.forEach(function(folder) {
            if((folder.name === selectedItemId)&&(folder.items.length === 0)) {
                isSubfolderNeeded = true;
            }
        });

        if (isSubfolderNeeded) {
            this.showTemplatesNames(component, event);
        } else {
            this.showTemplatesBody(component, event);
        }
    },

    showTemplatesNames : function (component, event) {
        var selectedItemId = event.getParam('name');

        var action = component.get("c.getTemplates");
        action.setParams({folderId : selectedItemId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                this.populateTemplateNames(component, event, response.getReturnValue(), selectedItemId);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    populateTemplateNames : function (component, event, response, selectedItemId) {
        var existedTemplateFolders = this.parseIfProxy(component.get('v.items'));

        existedTemplateFolders.forEach(function(folder) {
            if(folder.name === selectedItemId) {
                Object.keys(response).forEach(function(template){
                    folder.items.push({
                        "label" : response[template],
                        "name" : template,
                        "disabled" : false,
                        "expanded" : true,
                        "items": []
                    });
                });
            }
        });
        component.set('v.items', existedTemplateFolders);
    },

    showRecipientSelectList : function (component) {
        component.set('v.isrecipientNeedToBeChanged', true);
        component.set('v.isVisualforceTemplateToEdit', false);
    },

    showTemplatesBody : function (component, event) {
        component.set('v.isrecipientNeedToBeChanged', false);
        var selectedItemId = event.getParam('name');

        var action = component.get("c.getTemplateBody");
        action.setParams({templateId : selectedItemId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                if (response.getReturnValue().type === 'visualforce') {
                    this.prepareVFEditTemplate(component, event, response.getReturnValue());
                } else {
                    this.prepareTextEditTemplate(component);
                }
                this.populateDataForEditTemplate(component, event, response.getReturnValue(), selectedItemId);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    prepareVFEditTemplate : function (component, event, response) {
        component.set('v.isTemplateForEditShown', true);
        component.set('v.isVisualforceTemplateToEdit', true);
        component.set('v.selectedTemplatePopulatedType', response.recipient);
    },

    prepareTextEditTemplate : function (component) {
        component.set('v.isTemplateForEditShown', true);
        component.set('v.isVisualforceTemplateToEdit', false);
    },

    populateDataForEditTemplate : function (component, event, response, selectedItemId) {
        component.set('v.isTemplateForEditShown', true);
        component.find('subject').set('v.value', response.subject);
        component.find('name_edit').set('v.value', response.name);
        component.set('v.emailTemplate', response.body);

        var existedTemplateFolders = this.parseIfProxy(component.get('v.items'));
        var editedTemplate = [];

        existedTemplateFolders.forEach(function(folder) {
            folder.items.forEach(function(template){
                if(template.name === selectedItemId) {
                    editedTemplate.push({
                        "id" : template.name,
                        "name" : template.label
                    });
                    component.set('v.editedTemplate', editedTemplate);

                    if (response.isactive === 'true') {
                        component.find('checkbox_edit').set('v.value', true);
                    } else {
                        if (response.isactive === 'false') {
                            component.find('checkbox_edit').set('v.value', false);
                        }
                    }
                }
            });
        });
    },

    submitEditedTemplate : function (component, event) {
        var editableTemplateNameAndId = this.parseIfProxy(component.get('v.editedTemplate'));
        var editableTemplateBody = component.get('v.emailTemplate');
        var templateToUpdate = {};

        editableTemplateNameAndId.forEach(function(template) {
            templateToUpdate.id = template.id;
            templateToUpdate.name = component.find("name_edit").get("v.value");
            templateToUpdate.body = editableTemplateBody;
            templateToUpdate.isactive = component.find("checkbox_edit").get("v.value").toString();
            templateToUpdate.subject = component.find("subject").get("v.value");
        });

        if (!component.get('v.isVisualforceTemplateToEdit')) {
            this.saveUpdatedTemplate(component, templateToUpdate);
        } else {
            if (component.get('v.isrecipientNeedToBeChanged')) {
                templateToUpdate.recipient =  component.find("templ_recipient").get("v.value");
            } else {
                templateToUpdate.recipient = component.get('v.selectedTemplatePopulatedType');
            }
            this.saveUpdatedTemplate(component, templateToUpdate);
        }
    },

    saveUpdatedTemplate : function (component, templateToUpdate) {
        var action = component.get("c.updateTemplate");
        action.setParams({updatedTemplate : templateToUpdate});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.isrecipientNeedToBeChanged', false);
                this.showToast(component, event, "success", "Success!", "The template has been updated successfully.");
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    closeEditedTemplate : function (component) {
        component.set('v.isTemplateForEditShown', false);
        component.set('v.isVisualforceTemplateToEdit', false);
        component.set('v.isrecipientNeedToBeChanged', false);
    },

    closeCreatedTemplate : function (component) {
        component.set('v.isNewTemplateNeeded', false);
        component.set('v.isVisualforceTemplateToCreate', false);
    },

    closeEditTemplatePreview : function (component) {
        component.set('v.isEditPreviewNeeded', false);
    },

    closeCreateTemplatePreview : function (component) {
        component.set('v.isCreatePreviewNeeded', false);
        component.set('v.isNewTemplateNeeded', true);
    },

    parseIfProxy : function (response) {
        if (response) {
            return JSON.parse(JSON.stringify(response));
        }
        return response;
    },

    openCreateTemplateForm : function (component, event) {
        var selectedMenuItemValue = event.getParam("value");
        if (selectedMenuItemValue === 'Visualforce') {
            component.set('v.isVisualforceTemplateToCreate', true);
        }
        component.set('v.isNewTemplateNeeded', true);
        component.set('v.newEmailType', selectedMenuItemValue);
    },

    storeSelectedFolderForTemplate : function (component) {
        var selectedFolderName = component.find("selected_folder").get("v.value");
        var availebleFolders = this.parseIfProxy(component.get('v.availebleFolders'));

        availebleFolders.forEach(function(folder) {
            if (folder.name === selectedFolderName) {
                component.set('v.selectedFolderForTemplateId', folder.id);
                component.set('v.selectedFolderForTemplateName', folder.name);
            }
        });
    },

    submitCreatedTemplate : function (component, event) {
        var isTemplateTypeSelected = this.checkTemplateType(component, event);
        if (!isTemplateTypeSelected) {
            return;
        }

        var createdTemplate = {
            folderid : component.get('v.selectedFolderForTemplateId'),
            type : component.get('v.newEmailType'),
            name : component.get('v.selectedFolderForTemplateName'),
            subject : component.find("subject").get("v.value"),
            isactive : component.find("checkbox_isactive").get("v.value").toString(),
            body : component.get('v.createdEmailTemplate')
        };

        if (!component.get('v.isVisualforceTemplateToCreate')) {
            this.createNewTemplate(component, createdTemplate);
        } else {
            createdTemplate.recipient =  component.find("recipient").get("v.value");
            this.createNewTemplate(component, createdTemplate);
        }
    },

    createNewTemplate : function (component, createdTemplate) {
        var action = component.get("c.createTemplate");
        action.setParams({newTemplate : createdTemplate});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                this.showToast(component, event, "success", "Success!", "The template has been created successfully.");
                component.set('v.isVisualforceTemplateToCreate', false);
                $A.get('e.force:refreshView').fire();
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    checkTemplateType : function (component, event) {
        if (component.get('v.selectedFolderForTemplateId') === null) {
            this.showToast(component, event, "error", "Error!", "Please, select a folder for the template!");
            return false;
        }
        return true;
    },

    deleteTemplateById : function (component, event) {
        var templateId = this.parseIfProxy(component.get('v.editedTemplate'))[0].id;  // check if id is single

        var action = component.get("c.deleteTemplate");
        action.setParams({templateId : templateId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.isTemplateForEditShown', false);
                this.showToast(component, event, "success", "Success!", "The template has been deleted successfully.");
                $A.get('e.force:refreshView').fire();
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    showToast : function(component, event, type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });
        toastEvent.fire();
    },

    previewEditedTemplate : function (component) {
        component.set('v.isEditPreviewNeeded', true);
        this.getRelatedRecords(component);
    },

    previewNewTemplate : function (component) {
        component.set('v.isNewTemplateNeeded', false);
        component.set('v.isCreatePreviewNeeded', true);
    },

    getRelatedRecords : function (component) {
        var action = component.get("c.getObjectNames");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.relatedObjects', response.getReturnValue());
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    getSelectedObjectRecords : function (component, event) {
        var selectedObject = component.find('related_record').get('v.value');
        var selectedRecipient = component.find('recipient_record').get('v.value');
        var eventSourceId = event.getSource().getLocalId();

        if (eventSourceId === 'related_record') {
            this.populateSelectedObjectRecords(component, selectedObject, 'v.relatedObjectRecords');
        } else {
            if (eventSourceId === 'recipient_record') {
                this.populateSelectedObjectRecords(component, selectedRecipient, 'v.relatedRecords');
            }
        }
    },

    populateSelectedObjectRecords : function (component, parameter, iterationObject) {
        var action = component.get("c.getMergeData");
        action.setParams({selectedObject : parameter});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var relatedObjectRecords = this.handleMergedData(component, event, response.getReturnValue());
                component.set(iterationObject, relatedObjectRecords);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);

    },

    handleMergedData : function (component, event, response) {
        var selectedObjectRecords = [];

        if (Object.keys(response).length === 0) {
            this.showToast(component, event, "error", "Error!", "No records found due to one of the following: " +
                                                                "There are no records of this type. " +
                                                                "You don't have permission to see any of the records.");
        } else {
            Object.keys(response).forEach(function (record) {
                selectedObjectRecords.push({
                "id" : record,
                "name" : response[record].Name
                });
            });
        }
        return selectedObjectRecords;
    },

    submitPreview : function (component, event) {
        var getTemplatePreviewParameters = {};
            getTemplatePreviewParameters.templateId = this.parseIfProxy(component.get('v.editedTemplate')[0].id);
            getTemplatePreviewParameters.recipientId = component.find('recipient_record_name').get('v.value');
            getTemplatePreviewParameters.relatedRecId = component.find('related_record_name').get('v.value');
            getTemplatePreviewParameters.testemail = component.find('email_for_test').get('v.value');
            getTemplatePreviewParameters.send = component.find('checkbox').get('v.value').toString();

        this.setPreviewTemplate(component, getTemplatePreviewParameters);
    },

    setPreviewTemplate : function (component, parameters) {
        var action = component.get("c.getTemplatePreview");
        action.setParams({renderData : parameters});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.emailTemplate', response.getReturnValue());
                this.showToast(component, event, "success", "Success!", "The template has been sended successfully.");
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(this.parseIfProxy(errors));
            }
        });
        $A.enqueueAction(action);
    }

})