({
	init : function (component, event, helper) {
        helper.getTemplateFolders(component);
    },

    handleSelect: function (component, event, helper) {
        helper.handleSelect(component, event);
    },

    submitEditedTemplate : function (component, event, helper) {
        helper.submitEditedTemplate(component, event);
    },

    closeEditedTemplate : function (component, event, helper) {
        helper.closeEditedTemplate(component);
    },

    closeCreatedTemplate : function (component, event, helper) {
        helper.closeCreatedTemplate(component);
    },

    handleButtonMenuSelect : function (component, event, helper) {
        helper.openCreateTemplateForm(component, event);

    },

    storeSelectedFolderForTemplate : function (component, event, helper) {
        helper.storeSelectedFolderForTemplate(component);
    },

    submitCreatedTemplate : function (component, event, helper) {
	    helper.submitCreatedTemplate(component, event);
    },

    populateEditChekboxValue : function (component, event, helper) {
        helper.populateEditChekboxValue(component, event);
    },

    showRecipientSelectList : function (component, event, helper) {
        helper.showRecipientSelectList(component);
    },

    deleteTemplateById : function (component, event, helper) {
        helper.deleteTemplateById(component, event);
    },

    previewNewTemplate : function (component, event, helper) {
        helper.previewNewTemplate(component);
    },

    previewEditedTemplate : function (component, event, helper) {
        helper.previewEditedTemplate(component);
    },

    closeEditTemplatePreview : function (component, event, helper) {
        helper.closeEditTemplatePreview(component);
    },

    closeCreateTemplatePreview : function (component, event, helper) {
        helper.closeCreateTemplatePreview(component);
    },

    getSelectedObjectRecords : function (component, event, helper) {
      helper.getSelectedObjectRecords(component, event);
    },

    submitPreview : function (component, event, helper) {
      helper.submitPreview(component, event);
    },



})